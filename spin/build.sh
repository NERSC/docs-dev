#!/bin/bash

docker build -f Dockerfile.base -t registry.nersc.gov/usg/docs-dev-base . && \
docker build -f Dockerfile.httpd -t registry.nersc.gov/usg/docs-dev . && \
docker build -f Dockerfile.render -t registry.nersc.gov/usg/docs-dev-render . && \
docker image push registry.nersc.gov/usg/docs-dev-base && \
docker image push registry.nersc.gov/usg/docs-dev && \
docker image push registry.nersc.gov/usg/docs-dev-render

