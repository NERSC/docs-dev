# NERSC Technical Documentation

This repository contains NERSC technical documentation written in Markdown
which is converted to html/css/js with the [mkdocs](http://www.mkdocs.org)
static site generator. The [theme](https://gitlab.com/NERSC/mkdocs-material) is
a fork of [mkdocs-material](https://github.com/squidfunk/mkdocs-material) with
NERSC customizations such as the colors.

## Running Docker to build the docs

You can also use Docker to build and preview the docs locally:

Build the dev image:
```bash
docker build -t mkdocs .
```

Run it via the included Dockerfile:
```bash
docker run -it -v"$PWD:/app" mkdocs bash
```

and then use the mkdocs commands:
```bash
mkdocs --verbose build
```

Now on your laptop (not in docker):
```bash
cd public
python3 -m http.server 8001
```

And open a browser to http://localhost:8001/